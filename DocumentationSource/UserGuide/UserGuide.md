# User Guide

This page is a guide for running the NarupaXR Front End & guiding users in how to set up the HTC Vive for use with NarupaXR in both single and multi-user mode, as described [in our 2018 Science Advances paper](http://advances.sciencemag.org/content/4/6/eaat2731) and shown [in this video](https://vimeo.com/244670465)

## Equipment List 

For an n-person VR lab (we've successfully tested a maximum of n=7), the parts required are as follows:

* n Windows 10 client PCs, each equipped with a VR capable graphics card. We recommend at minimum an NVIDIA GTX 1060
* n HTC Vive (or alternatively HTC Vive Pro) [headsets](https://www.vive.com/)
* an 8-port network switch like [this one](https://www.amazon.co.uk/gp/product/B00AFVB41S/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)
* 1 computational server which will operate as the 'force field engine', equipped with
    * a good GPU for codes like OpenMM which support GPU acceleration (for example, we have an NVIDIA 1080Ti)
    * a good CPU which supports multi-threaded force engines like DFTB (for example, we have an AMD Ryzen 7 1700X sAM4 3.8GHz)
    * plenty of disk space for storing real-time trajectory data (how much storage you want depends on your applications)
    * you may optionally want to equip this machine with 2 boot disks, giving you the option to run force engines in either Windows10 or Linux
* n + 1 LAN cables (to connect all client and server computers to the network switch)

For each computer on which you are running NarupaXR, you will need the following: 

* An installation of [Steam](http://store.steampowered.com/about/).
* A Steam [login](https://store.steampowered.com/join/?redir=about%2F) (you can use the same one on all computers).
* An installation of the NarupaXR front end. 

## Setting up a VR installation

The following instructions are for setting up a multiuser installation of NarupaXR in the same shared physical space: 
1. Install a single pair of Vive lighthouses in the room where you want to run NarupaXR
2. On each client machine, connect and set up an HTC Vive [following these instructions](https://support.steampowered.com/steamvr/HTC_Vive/) and install NarupaXR.
3. Using the LAN cables, connect each machine (clients & servers) to the network switch. 
4. So as to get best performance, ensure that the machines (clients & servers) are not connected to another Wifi network (e.g. eduroam or whatever). 
5. On the server machine, start up a Narupa force engine server.
6. On each client machine, run the NarupaXR front end & connect to the force engine server. 
7. You should be able to see each player’s headsets and controllers in VR, and interact with whatever simulation is running on the force server. 

## Using NarupaXR 

When the application is first launched, you will be presented with a main menu screen in the virtual space: 

<img src="resources/narupa_vrmainmenu.png" alt="Narupa XR Main Menu." style="width: 300px;"/>

From here, you can connect to a simulation running on the local network, or configure settings by pointing the controller with the laser pointer on it at the button and pulling the trigger.

Upon clicking browse servers, you will be presented with all the servers running on the local network. 

**Troubleshooting** 

If there no servers are appearing, check that you are on the same network as the server you are expecting to see. On Windows, ensure that the local network you are on is configured to be a Private network. 

In the options menu, you can change the following settings: 

* Local Multiplayer: If enabled, the simulation box will be aligned such that all users share the same experience. 
* User Mode: If set to novice mode, then you will have basic UI features. These include switching between a few preset renderers, and pausing, playing and resetting the simulation. 
* Lighthouse Indexes: If local multiplayer is enabled, the lighthouses are used to coordinate the space for all players. There are 16 device indexes used by SteamVR devices (including the headset, controllers and other trackers). The app should automatically correctly identify the lighthouses. However, if this fails, you can use adjust the lighthouse indexes until the multiplayer looks right (with the simulation box in the middle of your space, and other players being rendered in their physical locations). 
* Enable Audio: If enabled, this turns on Unity's Audio engine. The corresponding flag can be found in the editor by going to edit/project_settings/audio.

Once you are connected to a server, the main menu will also present the option to load new simulations. Within this menu, there is a selection of preconfigured demos that you can run. 

## Advanced Mode 

In advanced mode, a whole suite of additional features become available. These are centered around the concept of layers. Instead of a single rendering mode, and only grabbing atoms one at a time, you can create layers out of subsets of the atoms, and customize how they look and how you interact with them. 

The controller UI takes on a different appearance in advanced mode. At the top of the panel, you can switch from Interaction Mode to Selection Mode. In Interaction Mode, the simulation is running and you can manipulate atoms. In Selection Mode, the simulation is paused and you can select atoms to create new layers. Upon pressing Selection Mode for the first time, a new empty selection is created, and the panel switches to the following layout: 

<img src="resources/narupa_vrselection_mode.png" alt="Selection Mode." style="width: 300px;"/>

There are a number of tools to facilitate the selection of atoms. Atoms can be selected by reaching towards them in the space and pulling the trigger, and can be selected as groups or chains. There are also menus for selecting by residue number, or just backbone atoms. 

By clicking on the Layers Menu button, you can add, remove and configure all the layers you have created: 

<img src="resources/narupa_vrlayers_menu.png" alt="Layers Menu." style="width: 300px;"/>

There is a base layer which represents the default visualisation layer that cannot be removed. It can however, be hidden or adjusted. 

By selecting a layer within this menu, you can adjust the style and colour with which it is represented, and change how you interact with it. The following interaction modes are available: 

* None - This layer is not interactable. 
* Atom - Interaction forces are applied to single atoms. 
* Group - Interaction forces are applied to the whole selection as a group. The center of mass of the group of atoms is used as the target of the interaction. This mode allows you to move whole groups of atoms while minimally affecting the structure. 

