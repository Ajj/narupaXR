﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Screens.Main;
using Narupa.VR.Player.Screens.Options;
using Narupa.VR.Player.Screens.Replay;
using Narupa.VR.Player.Screens.Server;
using Narupa.VR.UI;
using TMPro;
using UnityEngine;

namespace Narupa.VR.Player.Screens.Browsing
{
    /// <summary>
    /// Class for controlling transitions between different elements of the main menu.
    /// </summary>
    public class BrowsingUiController : MonoBehaviour
    {
        [SerializeField] private GameObject disconnectionScreen;

        [SerializeField] private TextMeshProUGUI headerText;

        private LocalSimulationSelectionPanel localSimulationSelectionPanel;
        private MainMenuController mainMenuController;
        private OptionsPanel optionsPanel;

        private VrReplaySelectionPanel replaySelectionPanel;
        private VrServerSelectionPanel serverSelectionPanel;

        // Use this for initialization
        private void Awake()
        {
            replaySelectionPanel = GetComponentInChildren<VrReplaySelectionPanel>();
            serverSelectionPanel = GetComponentInChildren<VrServerSelectionPanel>();
            optionsPanel = GetComponentInChildren<OptionsPanel>();
            headerText = GetComponentInChildren<TextMeshProUGUI>();
            localSimulationSelectionPanel = GetComponentInChildren<LocalSimulationSelectionPanel>();
            mainMenuController = FindObjectOfType<MainMenuController>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) BackToMainMenu();
        }

        /// <summary>
        /// Enters the server selection screen.
        /// </summary>
        public void EnterServerSelection()
        {
            HideAll();
            serverSelectionPanel.gameObject.SetActive(true);
            headerText.text = "Available Servers";
        }

        /// <summary>
        /// Enters the option screen.
        /// </summary>
        public void EnterOptionsPanel()
        {
            HideAll();
            headerText.text = "Options";
            optionsPanel.gameObject.SetActive(true);
        }

        /// <summary>
        /// Hides all browsing UI.
        /// </summary>
        public void HideAll()
        {
            if (replaySelectionPanel != null) replaySelectionPanel.gameObject.SetActive(false);
            if (serverSelectionPanel != null) serverSelectionPanel.gameObject.SetActive(false);
            if (disconnectionScreen != null) disconnectionScreen.gameObject.SetActive(false);
            if (localSimulationSelectionPanel != null) localSimulationSelectionPanel.gameObject.SetActive(false);
            if (optionsPanel != null) optionsPanel.gameObject.SetActive(false);
        }

        /// <summary>
        /// Returns to main menu screen.
        /// </summary>
        public void BackToMainMenu()
        {
            gameObject.SetActive(false);
            HideAll();
            mainMenuController.EnableMainMenu();
        }

        /// <summary>
        /// Enters the direct connection screen.
        /// </summary>
        public void EnterDirectConnection()
        {
            HideAll();
            headerText.text = "Connect Directly to Server";
        }

        /// <summary>
        /// Hides browsing UI.
        /// </summary>
        public void HideBrowsingUi()
        {
            HideAll();
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Enter the simulation selection screen.
        /// </summary>
        public void EnterSimulationSelection()
        {
            HideAll();
            localSimulationSelectionPanel.gameObject.SetActive(true);
            headerText.text = "Simulations Available On Server";
        }
    }
}