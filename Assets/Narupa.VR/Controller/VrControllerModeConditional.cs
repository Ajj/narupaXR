﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using UnityEngine;

namespace Narupa.VR.Controller
{
	
	/// <summary>
	/// 	Conditionally activates/deactivates children when a mode is activated
	/// </summary>
	public class VrControllerModeConditional : MonoBehaviour
	{

		/// <summary>
		/// 	The modes which this object is active for
		/// </summary>
		public List<ControllerMode> TriggerModes;

		private VrControllerStateManager controllerStateManager;

		// Use this for initialization
		void Awake()
		{
			this.controllerStateManager = FindObjectOfType<VrControllerStateManager>();
			controllerStateManager.ModeStart += ControllerStateManagerOnModeStart;
		}

		/// <summary>
		/// 	Called when the mode is changed, activating/deactivating all children
		/// </summary>
		private void ControllerStateManagerOnModeStart(object sender, VrControllerStateManager.ControllerModeChangedEventArgs e)
		{
			bool val = GetValue();
			for(int i = 0; i < transform.childCount; i++)
				transform.GetChild(i).gameObject.SetActive(val);
		}

		/// <summary>
		/// 	Check whether the conditional is currently active
		/// </summary>
		public bool GetValue()
		{
			return TriggerModes.Contains(controllerStateManager.ActiveMode);
		}
	}
}
