﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using Narupa.Client.UI.Pointer;
using Narupa.VR.UI.Pointer;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
    /// <inheritdoc />
    /// <summary>
    ///     Class for animating the entry and exit of VR canvases.
    /// </summary>
    public class VrCanvas : MonoBehaviour
    {
        public float AppearanceTime = 0.1f;
        public iTween.EaseType EaseType = iTween.EaseType.easeInQuad;
        private bool initialised;
        private Vector3 localScale;

        private readonly Vector3 nearZero = 0.000001f * Vector3.one;

        private void Start()
        {
            if (initialised == false) Initialise();
        }

        private void Initialise()
        {
            localScale = gameObject.transform.localScale;
            initialised = true;
            
            // Automatically replace GraphicRaycaster with a raycaster suitable for VR
            foreach (var graphicRaycaster in this.gameObject.GetComponents<GraphicRaycaster>())
            {
                graphicRaycaster.enabled = false;
                if(graphicRaycaster.gameObject.GetComponent<UiPointerRaycaster>() == null)
                    graphicRaycaster.gameObject.AddComponent<UiPointerRaycaster>();
            }
        }

        /// <summary>
        ///     Scales up the canvas.
        /// </summary>
        private void OnEnable()
        {
            if (initialised == false) Initialise();
            var hash = iTween.Hash("scale", nearZero, "time", AppearanceTime, "easetype", EaseType);
            gameObject.transform.localScale = localScale;
            iTween.ScaleFrom(gameObject, hash);
        }

        /// <summary>
        ///     Scales down the attached gameobject, then disables it.
        /// </summary>
        /// <returns></returns>
        public IEnumerator DisableRoutine(float disableTime = 0.1f)
        {
            if (initialised == false) Initialise();
            var hash = iTween.Hash("scale", nearZero, "time", disableTime, "easetype", EaseType);
            gameObject.transform.localScale = localScale;
            iTween.ScaleTo(gameObject, hash);
            yield return new WaitForSeconds(disableTime + 0.02f);
            iTween.Stop(gameObject);
            gameObject.SetActive(false);
            //Force scale back to original scale.
            gameObject.transform.localScale = localScale;
        }
    }
}