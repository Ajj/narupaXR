﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using Narupa.VR.UI.Primitive;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Narupa.VR.UI.Radial
{
    /// <summary>
    ///     Radial menu, based on version in VRTK
    /// </summary>
    [ExecuteInEditMode]
    public class UiRadialMenu : UIBehaviour
    {
        [System.Serializable]
        public class ButtonDefinition
        {
            [SerializeField] public Sprite Sprite;
            [SerializeField] public UnityEvent OnClick = new UnityEvent();
        }
        
        public class RadialButtonEventArgs : EventArgs
        {
            public readonly GameObject Button;
            public readonly int ButtonIndex;

            public RadialButtonEventArgs(GameObject button, int index)
            {
                this.Button = button;
                this.ButtonIndex = index;
            }
        }
        
        public class RadialButtonChangeEventArgs : EventArgs
        {
            public readonly GameObject OldButton;
            public readonly int OldButtonIndex;
            public readonly GameObject NewButton;
            public readonly int NewButtonIndex;

            public RadialButtonChangeEventArgs(GameObject oldButton, int oldIndex, GameObject newButton, int newIndex)
            {
                this.OldButton = oldButton;
                this.OldButtonIndex = oldIndex;
                this.NewButton = newButton;
                this.NewButtonIndex = newIndex;
            }
        }

        public enum ButtonEvent
        {
            hoverupdate,
            hoverOff,
            clickdown,
            clickup
        }

        public event EventHandler<RadialButtonEventArgs> ButtonClick;
        public event EventHandler<RadialButtonEventArgs> ButtonDown;
        public event EventHandler<RadialButtonEventArgs> ButtonUp;
        public event EventHandler<RadialButtonEventArgs> ButtonExit;
        public event EventHandler<RadialButtonEventArgs> ButtonEnter;

        public event EventHandler<RadialButtonChangeEventArgs> HoverChange;

        
        public event EventHandler MenuEnter;
        public event EventHandler MenuExit;

        private void OnButtonClick(GameObject button, PointerEventData pointer)
        {
            OnButtonInteract(button, pointer, ExecuteEvents.pointerClickHandler, ButtonClick);
            buttons[GetButtonIndexFromButton(button)].OnClick.Invoke();
        }
        
        private void OnButtonDown(GameObject button, PointerEventData pointer)
        {
            OnButtonInteract(button, pointer, ExecuteEvents.pointerDownHandler, ButtonDown);
        }
        
        private void OnButtonUp(GameObject button, PointerEventData pointer)
        {
            OnButtonInteract(button, pointer, ExecuteEvents.pointerUpHandler, ButtonUp);
        }
        
        private void OnButtonEnter(GameObject button, PointerEventData pointer)
        {
            OnButtonInteract(button, pointer, ExecuteEvents.pointerEnterHandler, ButtonEnter);
        }
        
        private void OnButtonExit(GameObject button, PointerEventData pointer)
        {
            OnButtonInteract(button, pointer, ExecuteEvents.pointerExitHandler, ButtonExit);
        }
        
        
        private void OnButtonInteract<T>(GameObject button, PointerEventData pointer, 
            ExecuteEvents.EventFunction<T> handler, EventHandler<RadialButtonEventArgs> evnt) where T : IEventSystemHandler
        {
            ExecuteEvents.Execute(button, pointer, handler);
            evnt?.Invoke(this, new RadialButtonEventArgs(button, GetButtonIndexFromButton(button)));
        }
        
        private void OnMenuEnter()
        {
            MenuEnter?.Invoke(this, null);
        }
        
        private void OnMenuExit()
        {
            MenuExit?.Invoke(this, null);
        }
        

        [SerializeField] private List<ButtonDefinition> buttons;

        [SerializeField] private GameObject buttonPrefab;
        
        [SerializeField] private bool generateOnAwake = true;

        [Tooltip("The additional rotation of the Radial Menu.")]
        [Range(0, 359)]
        [SerializeField] private float offsetRotation;

        [Tooltip("Whether button icons should rotate according to their arc or be vertical compared to the controller.")]
        [SerializeField] private bool rotateIcons;

        [Tooltip("Whether the buttons are shown")]
        [SerializeField] private bool isShown;

        [Tooltip("Hide the menu when not in use")]
        [SerializeField] private bool hideOnRelease;

        [Header("Visuals")]
        [SerializeField] private bool fill;
        [SerializeField] private float thickness;

        //Has to be public to keep state from editor -> play mode?
        [Tooltip("The actual GameObjects that make up the radial menu.")]
        [SerializeField] private List<GameObject> menuButtons;

        private const int indexNone = -1;
        
        protected int currentHoverIndex = indexNone;
        protected int currentClickIndex = indexNone;
        

        public virtual void EndHover()
        {
            var pointer = new PointerEventData(EventSystem.current);
            
            // If a button is being hovered over, stop this
            if (currentHoverButton != null)
            {
                this.OnButtonExit(currentHoverButton, pointer);
                this.OnMenuExit();
                currentHoverButton = null;
            }
            
            // If a button is being pressed, end this
            if (currentClickButton != null)
            {
                this.OnButtonUp(currentClickButton, pointer);
                currentClickButton = null;
            }
        }

        /// <summary>
        /// The HoverButton method is used to set the button hover at a given angle.
        /// </summary>
        /// <param name="angle">The angle on the radial menu.</param>
        public virtual void UpdateHover(float angle)
        {
            InteractButton(angle, ButtonEvent.hoverupdate);
        }

        /// <summary>
        /// The ClickButton method is used to set the button click at a given angle.
        /// </summary>
        /// <param name="angle">The angle on the radial menu.</param>
        public virtual void ClickDown(float angle)
        {
            InteractButton(angle, ButtonEvent.clickdown);
        }

        /// <summary>
        /// The UnClickButton method is used to set the button unclick at a given angle.
        /// </summary>
        /// <param name="angle">The angle on the radial menu.</param>
        public virtual void ClickUp(float angle)
        {
            InteractButton(angle, ButtonEvent.clickup);
        }

        /// <summary>
        /// The ToggleMenu method is used to show or hide the radial menu.
        /// </summary>
        public virtual void ToggleMenu()
        {
            if (isShown)
            {
                HideMenu(true);
            }
            else
            {
                ShowMenu();
            }
        }

        public GameObject currentHoverButton
        {
            get { return currentHoverIndex == indexNone ? null : this.menuButtons[currentHoverIndex]; }
            set { this.currentHoverIndex = this.menuButtons.IndexOf(value); }
        }
        
        public GameObject currentClickButton
        {
            get { return currentClickIndex == indexNone ? null : this.menuButtons[currentClickIndex]; }
            set { this.currentClickIndex = this.menuButtons.IndexOf(value); }
        }

        public void OnHoverChange(GameObject old, GameObject nw)
        {
            this.HoverChange?.Invoke(this, new RadialButtonChangeEventArgs(old, GetButtonIndexFromButton(old), 
                nw, GetButtonIndexFromButton(nw)));
        }

        /// <summary>
        /// The ShowMenu method is used to show the menu.
        /// </summary>
        public virtual void ShowMenu()
        {
            if (!isShown)
            {
                isShown = true;
                StopCoroutine("TweenMenuScale");
                StartCoroutine("TweenMenuScale", isShown);
            }
        }

        /// <summary>
        /// The GetButton method is used to get a button from the menu.
        /// </summary>
        /// <param name="id">The id of the button to retrieve.</param>
        /// <returns>The found radial menu button.</returns>
        public virtual ButtonDefinition GetButton(int id)
        {
            if (id < buttons.Count)
            {
                return buttons[id];
            }
            return null;
        }
        
        public int GetButtonIndexFromButton(GameObject button)
        {
            return this.menuButtons.IndexOf(button);
        }


        /// <summary>
        /// The HideMenu method is used to hide the menu.
        /// </summary>
        /// <param name="force">If true then the menu is always hidden.</param>
        public virtual void HideMenu(bool force)
        {
            if (isShown && (hideOnRelease || force))
            {
                isShown = false;
                StopCoroutine("TweenMenuScale");
                StartCoroutine("TweenMenuScale", isShown);
            }
        }

        /// <summary>
        /// The RegenerateButtons method creates all the button arcs and populates them with desired icons.
        /// </summary>
        [ContextMenu("Regenerate")]
        public void RegenerateButtons()
        {
            RemoveAllButtons();
            for (int i = 0; i < buttons.Count; i++)
            {
                // Initial placement/instantiation
                GameObject newButton = Instantiate(buttonPrefab);
                newButton.transform.SetParent(transform);
                newButton.transform.localScale = Vector3.one;
                newButton.GetComponent<RectTransform>().offsetMax = Vector2.zero;
                newButton.GetComponent<RectTransform>().offsetMin = Vector2.zero;

                //Setup button arc
                UiCircle circle = newButton.GetComponent<UiCircle>();
                float fillPerc = 1f / buttons.Count;
                circle.FillAmount = fillPerc;

                circle.Fill = this.fill;
                circle.Thickness = this.thickness;

                //Final placement/rotation
                float angle = ((1f / buttons.Count) * i) * 360f + offsetRotation;
                newButton.transform.localEulerAngles = new Vector3(0, 0, angle);
                newButton.layer = 4; //UI Layer
                newButton.transform.localPosition = Vector3.zero;
                

                //Place and populate Button Icon
                GameObject buttonIcon = newButton.GetComponentInChildren<Image>().gameObject;
                if (buttons[i].Sprite == null)
                {
                    buttonIcon.SetActive(false);
                }
                else
                {
                    
                    buttonIcon.GetComponent<Image>().sprite = buttons[i].Sprite;
                    /*
                    buttonIcon.transform.localPosition = new Vector2(-1 * ((newButton.GetComponent<RectTransform>().rect.width / 2f) - (circle.thickness / 2f)), 0);
                    //Min icon size from thickness and arc
                    float scale1 = Mathf.Abs(circle.thickness);
                    float R = Mathf.Abs(buttonIcon.transform.localPosition.x);
                    float bAngle = (359f * circle.fillAmount * 0.01f * Mathf.PI) / 180f;
                    float scale2 = (R * 2 * Mathf.Sin(bAngle / 2f));
                    if (circle.fillAmount > 24) //Scale calc doesn't work for > 90 degrees
                    {
                        scale2 = float.MaxValue;
                    }

                    float iconScale = Mathf.Min(scale1, scale2) - iconMargin;
                    buttonIcon.GetComponent<RectTransform>().sizeDelta = new Vector2(iconScale, iconScale);
                    //Rotate icons all vertically if desired
                    */
                    if (!rotateIcons)
                    {
                        buttonIcon.transform.eulerAngles = GetComponentInParent<Canvas>().transform.eulerAngles;
                    }
                }
                menuButtons.Add(newButton);
            }
        }

        /// <summary>
        /// The AddButton method is used to add a new button to the menu.
        /// </summary>
        /// <param name="newButton">The button to add.</param>
        public void AddButton(ButtonDefinition newButton)
        {
            buttons.Add(newButton);
            RegenerateButtons();
        }

        public int ButtonCount
        {
            get { return buttons.Count; }
        }

        protected override void Awake()
        {
            base.Awake();
            if (Application.isPlaying)
            {
                if (!isShown)
                {
                    transform.localScale = Vector3.zero;
                }
                if (generateOnAwake)
                {
                    RegenerateButtons();
                }
            }
        }

        #if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            if (isShown)
            {
                ShowMenu();
                transform.localScale = Vector3.one;
            }
            else
            {
                HideMenu(true);
            }
        }
        #endif

        /// <summary>
        ///     Converts an angle to a button index
        /// </summary>
        public int GetButtonIndex(float angle)
        {
            //Get button ID from angle
            float buttonAngle = 360f / buttons.Count; //Each button is an arc with this angle
            angle = ((int)(angle + -offsetRotation)) %  360; //Offset the touch coordinate with our offset

            return ((int)(((angle + (buttonAngle / 2f)) / buttonAngle))) % buttons.Count; //Convert angle into ButtonID (This is the magic)

        }

        public GameObject GetButtonFromAngle(float angle)
        {
            return menuButtons[GetButtonIndex(angle)];
        }

        //Turns and Angle and Event type into a button action
        protected virtual void InteractButton(float angle, ButtonEvent evt) //Can't pass ExecuteEvents as parameter? Unity gives error
        {
            var pointer = new PointerEventData(EventSystem.current);

            var currentButton = GetButtonFromAngle(angle);
            
            // If previous hover button is no longer being hovered
            if (currentHoverButton != null && currentHoverButton != currentButton)
            {
                OnButtonExit(currentHoverButton, pointer);
            }
            
            if (evt == ButtonEvent.hoverupdate && currentHoverButton != currentButton) // Show hover UI event (darken button etc). Show menu
            {
                if (currentHoverButton != null)
                {
                    OnButtonExit(currentHoverButton, pointer);
                }
                else if(currentHoverButton == null)
                    MenuEnter?.Invoke(this, null);
                
                if(currentHoverButton != null && currentButton != null)
                    OnHoverChange(currentHoverButton, currentButton);

                currentHoverButton = currentButton;
                OnButtonEnter(currentHoverButton, pointer);
            }
            else
            if (evt == ButtonEvent.clickdown)
            {
                OnButtonDown(currentButton, pointer);
                OnButtonClick(currentButton, pointer);
            }
            else if (evt == ButtonEvent.clickup) //Clear press id to stop invoking OnHold method (hide menu)
            {
               OnButtonUp(currentButton, pointer);
                currentClickButton = null;
            }
            
        }

        //Simple tweening for menu, scales linearly from 0 to 1 and 1 to 0
        protected virtual IEnumerator TweenMenuScale(bool show)
        {
            float targetScale = 0;
            Vector3 Dir = -1 * Vector3.one;
            if (show)
            {
                targetScale = 1;
                Dir = Vector3.one;
            }
            int i = 0; //Sanity check for infinite loops
            while (i < 250 && ((show && transform.localScale.x < targetScale) || (!show && transform.localScale.x > targetScale)))
            {
                transform.localScale += Dir * Time.deltaTime * 4f; //Tweening function - currently 0.25 second linear
                yield return true;
                i++;
            }
            transform.localScale = Dir * targetScale;
            StopCoroutine("TweenMenuScale");
        }

        protected virtual void RemoveAllButtons()
        {
            if (menuButtons == null)
            {
                menuButtons = new List<GameObject>();
            }
            for (int i = 0; i < menuButtons.Count; i++)
            {
                DestroyImmediate(menuButtons[i]);
            }
            menuButtons = new List<GameObject>();
        }
    }
}