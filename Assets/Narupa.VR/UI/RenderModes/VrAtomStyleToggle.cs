﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Renderers;
using Narupa.VR.UI.Buttons;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.RenderModes
{
    /// <summary>
    ///     Class for switching between different atom styles.
    /// </summary>
    public class VrAtomStyleToggle : MonoBehaviour
    {
        public enum AtomStyle
        {
            /// <summary>
            ///     Display no atoms.
            /// </summary>
            NoAtom,

            /// <summary>
            ///     Display atoms at normal radius.
            /// </summary>
            Normal,

            /// <summary>
            ///     Display atoms at VDW radius.
            /// </summary>
            Vdw,

            /// <summary>
            ///     Displays atoms with trails.
            /// </summary>
            Wow
        }

        [SerializeField] private NsbOutline nsbRepresentation;
        [SerializeField] private NsbWow nsbWow;

        [SerializeField] private RendererManager rendererManager;

        [SerializeField] private AtomStyle style = AtomStyle.Normal;

        [SerializeField] 
        private StyleSource hueStyle;

        [SerializeField] private StyleSource cpkStyle;

        private Toggle toggle;
        
        
        protected void Awake()
        {
            GetComponent<UiToggleButton>().OnToggleOn.AddListener(OnToggleOn);
            toggle = GetComponent<Toggle>();
        }

        protected void Start()
        {
            if(toggle.isOn)
                OnToggleOn();
        }

        private void OnEnable()
        {
            if (style == AtomStyle.Wow && rendererManager.ActiveRenderer == nsbWow) toggle.isOn = true;
            else if (nsbRepresentation.NsbRepresentation.Settings.Mode.Value == (int) style) toggle.isOn = true;
            else toggle.isOn = false;
        }

        private void OnToggleOn(object sender = null, UiToggleButton.ToggleButtonEventArgs args = null)
        {
            switch (style)
            {
                case AtomStyle.NoAtom:
                    rendererManager.SetRenderer(nsbRepresentation);
                    rendererManager.SetRenderStyle(cpkStyle.Component.Style);
                    nsbRepresentation.NsbRepresentation.Settings.Mode.Value = 0;
                    break;

                case AtomStyle.Normal:
                    rendererManager.SetRenderer(nsbRepresentation);
                    rendererManager.SetRenderStyle(cpkStyle.Component.Style);
                    nsbRepresentation.NsbRepresentation.Settings.Mode.Value = 1;
                    break;

                case AtomStyle.Vdw:
                    rendererManager.SetRenderer(nsbRepresentation);
                    rendererManager.SetRenderStyle(cpkStyle.Component.Style);
                    nsbRepresentation.NsbRepresentation.Settings.Mode.Value = 2;
                    break;
                case AtomStyle.Wow:
                    rendererManager.SetRenderer(nsbWow);
                    rendererManager.SetRenderStyle(hueStyle.Component.Style);
                    break;
                default:
                    return;
            }
        }
    }
}