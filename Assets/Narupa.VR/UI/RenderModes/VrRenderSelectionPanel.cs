﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Selection;
using NSB.MMD.Base;
using NSB.Utility;
using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.RenderModes
{
    public class VrRenderSelectionPanel : VrCanvas
    {
        private InstancePool<MMDRenderer> availableRenderers;


        /// <summary>
        ///     Renderers associated with the default layer.
        /// </summary>
        private RendererManager defaultRenderers;

        [SerializeField] private InstancePoolSetup rendererSelectionSetup;

        [SerializeField] private TextMeshProUGUI selectedRendererText;


        private SelectionManager selectionManager;

        // Use this for initialization
        private void Awake()
        {
            availableRenderers = rendererSelectionSetup.Finalise<MMDRenderer>(false);
        }

        private void Start()
        {
            selectionManager = FindObjectOfType<SelectionManager>();
        }

        private void FindBaseLayerRenderManager()
        {
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            defaultRenderers = selectionManager.BaseLayerRendererManager;
        }

        private void OnEnable()
        {
            if (defaultRenderers == null) FindBaseLayerRenderManager();
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            if (selectionManager.ActiveSelection == null)
                availableRenderers.SetActive(defaultRenderers.Renderers);
            else
                availableRenderers.SetActive(selectionManager.ActiveSelection.RendererRoot.Renderers);
            //Subscribe to all the renderer buttons so we can close the menu when one is clicked.
            foreach (var button in GetComponentsInChildren<RendererButton>())
                button.RendererSelected += OnRendererSelected;
        }

        private void OnRendererSelected(object sender, EventArgs e)
        {
            var render = sender as MMDRenderer;
            if (render != null) selectedRendererText.text = render.gameObject.name;
            else selectedRendererText.text = "Invalid Renderer";
            gameObject.SetActive(false);
        }
    }
}