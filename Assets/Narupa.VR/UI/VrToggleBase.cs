﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using Narupa.VR.UI.Tooltips;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for toggles in VR.
    /// </summary>
    public class VrToggleBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] protected string Message;

        [SerializeField] private MessageTooltipManager messageTooltip;

        [Header("Tooltip Options")] [SerializeField]
        protected string Title;

        protected Toggle Toggle;

        public bool ToggleEnabled = true;
        private readonly float tooltipDelayTime = 0.2f;
        private uint toolTipId;
        
        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            StopCoroutine(StartTooltipDelay());
            StartCoroutine(StartTooltipDelay());
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            DestroyTooltip();
        }

        /// <summary>
        ///     Handler for when trigger is pulled while highlighted i.e. the button is clicked.
        /// </summary>
        public event EventHandler ToggleChangeValue;

        protected virtual void Awake()
        {
            Toggle = GetComponentInChildren<Toggle>();
            Toggle.onValueChanged.AddListener(v => OnClick(v));

            if (messageTooltip == null) messageTooltip = GetComponentInParent<MessageTooltipManager>();
            if (messageTooltip == null)
                Debug.LogWarning(
                    "Unable to find tooltip manager in parent for this button, tooltips will not be shown.");
        }

        protected virtual void OnClick(bool v, bool userTriggered = true)
        {
            if (ToggleChangeValue != null) ToggleChangeValue(v, null);
            //TODO: Reinstate using new haptic feedback
            /*
            if (userTriggered && uiPointer != null)
                VRTK_ControllerHaptics.TriggerHapticPulse(
                    VRTK_ControllerReference.GetControllerReference(uiPointer.gameObject),
                    VrButtonBase.HapticFeedBackStrength);
             */
        }

        private IEnumerator StartTooltipDelay()
        {
            yield return new WaitForSeconds(tooltipDelayTime);
            if (!string.IsNullOrEmpty(Title) && messageTooltip != null)
                toolTipId = messageTooltip.DisplayTooltip(transform, Title, Message);
        }

        private void DestroyTooltip()
        {
            StopCoroutine(StartTooltipDelay());
            messageTooltip?.HideTooltip(toolTipId);
        }

        private void OnDisable()
        {
            DestroyTooltip();
        }

        protected virtual void OnEnable()
        {
           
        }
    }
}