// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Narupa.VR.Multiplayer.PlayerMaterials
{
    /// <summary>
    /// Class for controlling colours in multiplayer.
    /// </summary>
    public class MultiplayerMaterialController : MonoBehaviour
    {
        private static readonly List<int> KnownPlayers = new List<int>();

        private readonly object locker = new object();

        [SerializeField] private Color[] playerColors;

        [SerializeField] public Color SelectionColor;

        public static event EventHandler PlayerColorChange;

        public Color GetPlayerColor(int playerId)
        {
            return playerColors[GetPlayerIndex(playerId)];
        }

        private int GetPlayerIndex(int playerId)
        {
            lock (locker)
            {
                if (KnownPlayers.Contains(playerId) == false)
                {
                    KnownPlayers.Add(playerId);
                    //Sort the players, resulting in consistent colourings.
                    KnownPlayers.Sort();
                    if (PlayerColorChange != null) PlayerColorChange(this, null);
                }

                return KnownPlayers.IndexOf(playerId) % playerColors.Length;
            }
        }
    }
}