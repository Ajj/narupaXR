﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    ///     Class for calibrating multiplayer Room-Scale SteamVR.
    /// </summary>
    public class MultiplayerSteamVrCalibration : MonoBehaviour
    {
        /// <summary>
        ///     Scaling applied to the multiplayer scene.
        /// </summary>
        public static Vector3 MultiplayerScale = Vector3.one;

        /// <summary>
        ///     Whether calibration is active or not.
        /// </summary>
        public bool Activated;

        /// <summary>
        ///     Whether to flip the lighthouse order.
        /// </summary>
        public bool FlipLighthouses;

        private int frameCount;

        /// <summary>
        ///     Adjustment made to the narupa transform to make it a good height in multiplayer VR.
        /// </summary>
        [SerializeField] [Tooltip("Height of narupa box in multiplayer VR.")]
        public float HeightAdjustment = 1f;

        /// <summary>
        ///     How many frames to delay until starting calibration.
        /// </summary>
        /// <remarks>
        ///     This gives steamVR / VRTK time to sort themselves out.
        /// </remarks>
        [SerializeField] private int initialisationDelay = 30;

        private bool initialised;

        /// <summary>
        ///     Transform of the first lighthouse.
        /// </summary>
        public Transform LighthouseA;

        /// <summary>
        ///     Transform of the second lighthouse.
        /// </summary>
        public Transform LighthouseB;

        [SerializeField] private float multiplayerScaleLerpSpeed = 2f;

        /// <summary>
        /// Measure of the physical space size.
        /// </summary>
        [Space(5)] [Header("Debug Properties")]
        public float PhysicalSpaceSize;

        /// <summary>
        ///     Transform of narupa root object.
        /// </summary>
        public Transform NarupaTransform;

        private bool vrDisabled;

        private bool TryDetermineLighthouseIndex()
        {
            var error = ETrackedPropertyError.TrackedProp_Success;
            var lightHouseIndexes = new List<uint>();
            var lightHouseSerialNumbers = new List<long>();
            for (uint i = 0; i < 16; i++)
            {
                var result = new StringBuilder(64);
                try
                {
                    OpenVR.System.GetStringTrackedDeviceProperty(i, ETrackedDeviceProperty.Prop_RenderModelName_String,
                        result, 64, ref error);
                }
                catch (NullReferenceException)
                {
                    Debug.LogWarning("OpenVR appears to not be initialized, cannot calibrate lighthouses!");
                    vrDisabled = true;
                }

                if (result.ToString().Contains("basestation"))
                {
                    var serialNumberString = new StringBuilder(64);
                    OpenVR.System.GetStringTrackedDeviceProperty(i, ETrackedDeviceProperty.Prop_SerialNumber_String,
                        serialNumberString,
                        64, ref error);
                    Debug.Log($"Basestation serial number: {serialNumberString}");
                    lightHouseIndexes.Add(i);
                    long serialNumber = 0;
                    try
                    {
                        serialNumber = long.Parse(serialNumberString.ToString().Split('-')[1], NumberStyles.HexNumber);
                    }
                    catch
                    {
                        Debug.Log("Unable to parse hex");
                    }

                    lightHouseSerialNumbers.Add(serialNumber);
                }
            }

            //Assign the first two lighthouses to be the reference points.
            if (lightHouseIndexes.Count < 2) return false;

            if (lightHouseSerialNumbers[0] < lightHouseSerialNumbers[1])
            {
                SetLightHouseIndex(LighthouseA, (int) lightHouseIndexes[0]);
                SetLightHouseIndex(LighthouseB, (int) lightHouseIndexes[1]);
            }
            else
            {
                SetLightHouseIndex(LighthouseA, (int) lightHouseIndexes[1]);
                SetLightHouseIndex(LighthouseB, (int) lightHouseIndexes[0]);
            }

            return true;
        }

        private void SetLightHouseIndex(Transform lighthouse, int availableDeviceIndex)
        {
            var device = lighthouse.GetComponentInChildren<SteamVR_TrackedObject>();
            device.index = (SteamVR_TrackedObject.EIndex) availableDeviceIndex;
        }

        // Update is called once per frame
        private void Update()
        {
            if (vrDisabled)
                return;

            if (!Activated)
                return;


            if (!initialised && frameCount > initialisationDelay)
            {
                if (TryDetermineLighthouseIndex())
                {
                    LighthouseA.gameObject.SetActive(true);
                    LighthouseB.gameObject.SetActive(true);
                    initialised = true;
                }
            }
            else if (!initialised)
            {
                frameCount++;
                return;
            }


            if (Input.GetKeyDown(KeyCode.Backspace)) FlipLighthouses = !FlipLighthouses;

            // try to resolve the transform for the narupa
            var lighthouseAPos = LighthouseA.position;
            var lighthouseBPos = LighthouseB.position;
            if (FlipLighthouses)
            {
                lighthouseBPos = LighthouseA.position;
                lighthouseAPos = LighthouseB.position;
            }

            PhysicalSpaceSize = Vector3.Distance(lighthouseAPos, lighthouseBPos);
            // find the angle from Vive space to narupa space
            var angleFromViveToNarupaSpace =
                Mathf.Atan2(lighthouseBPos.z - lighthouseAPos.z, lighthouseBPos.x - lighthouseAPos.x) * Mathf.Rad2Deg;
            // Align narupa to the Vive space.
            NarupaTransform.localRotation = Quaternion.AngleAxis(-angleFromViveToNarupaSpace, Vector3.up);
            var centerOffset = (lighthouseBPos + lighthouseAPos) * 0.5f;
            centerOffset.y = HeightAdjustment;
            NarupaTransform.position = centerOffset;
            NarupaTransform.localScale = Vector3.Lerp(NarupaTransform.localScale, MultiplayerScale,
                multiplayerScaleLerpSpeed * Time.deltaTime);
        }
    }
}