﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Player.Screens.Options;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    /// Class for handling lighthouse index selection in the options menu.
    /// </summary>
    [RequireComponent(typeof(OptionUiCycler))]
    public class LighthouseIndexControl : MonoBehaviour
    {
        [SerializeField] private SteamVR_TrackedObject lighthouse;

        [SerializeField] private SteamVR_TrackedObject.EIndex lightHouseIndex = SteamVR_TrackedObject.EIndex.Device3;

        private OptionUiCycler optionCycler;

        [SerializeField] private string optionLabel = "Lighthouse Index";

        private void Awake()
        {
            optionCycler = GetComponent<OptionUiCycler>();

            optionCycler.OptionChanged += SetIndex;

            //This didn't seem to work well in editor?
            /*
        if (PlayerPrefs.HasKey(optionLabel))

        {
            var multInt = PlayerPrefs.GetInt(optionLabel);

            lightHouseIndex = (SteamVR_TrackedObject.EIndex)multInt;
        }

        lighthouse.index = lightHouseIndex;
        */
            optionCycler.RegisterOption<SteamVR_TrackedObject.EIndex>(optionLabel);
        }

        private void OnEnable()
        {
            lightHouseIndex = lighthouse.index;
            optionCycler.SetOption((int) lightHouseIndex);
        }

        /// <summary>
        /// Sets the index of the lighthouse.
        /// </summary>
        /// <param name="index">The lighthouse index.</param>
        public void SetIndex(SteamVR_TrackedObject.EIndex index)
        {
            lightHouseIndex = index;
            lighthouse.index = index;
            var i = (int) index;
            PlayerPrefs.SetInt(optionLabel, i);
        }

        /// <summary>
        /// Sets the index of the lighthouse.
        /// </summary>
        /// <param name="sender">Index of the lighthouse.</param>
        /// <param name="e">EventArgs</param>
        /// <exception cref="Exception">"Failed to parse lighthouse index."</exception>
        public void SetIndex(object sender, EventArgs e = null)

        {
            var enumStr = sender as string;
            if (enumStr == null)
                throw new Exception("Failed to set index of lighthouse, as object received was not a string");
            SteamVR_TrackedObject.EIndex index;
            var result = Enum.TryParse(enumStr, true, out index);
            if (!result) throw new Exception("Failed to parse lighthouse index");
            SetIndex(index);
        }
    }
}