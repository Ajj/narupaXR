// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Sonification.Unused
{
    [RequireComponent(typeof(AudioSource))]
    public class SetStartTime : MonoBehaviour
    {
        public float startTime = 0.0f;

        // Use this for initialization
        void Start()
        {
            var s = GetComponent<AudioSource>();
            s.time = startTime;
            s.Play();
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}
