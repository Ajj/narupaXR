﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Narupa.Client.Network;

namespace Narupa.Client.FileSystem
{
    /// <summary>
    /// An IFileSystem implementation that has files manually added to it using AddFile. For use with servers.
    /// </summary>
    public class DiscoverableFileSystem : IFileSystem
    {
        private List<string> files = new List<string>();
    
        public IEnumerable<string> GetDirectories(string path)
        {
            return new string[0];
        }

        public IEnumerable<string> GetFiles(string path)
        {
            return files;
        }

        public void AddFile(string file)
        {
            files.Add(file);
        }

    }
}
