// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Client.UI.Pointer
{
    /// <summary>
    /// Allows other classes to influence the behaviour of a UI Pointer. Register with UiPointerInputModule.AddOverride
    /// </summary>
    public interface IUiPointerOverride
    {
        /// <summary>
        /// Override and return false to conditionally disable the UI pointer
        /// </summary>
        bool IsUiPointerActive { get; }
    }
}