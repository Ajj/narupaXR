﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using UnityEngine;

namespace NSB.MMD.Base
{
    /// <summary>
    /// Represents a renderer that uses data from a Frame, Selection, and Style to 
    /// render a molecule when Refresh is called. Further molecule-agnostic configuration 
    /// can be listed and modified via a Config.
    /// </summary>
    public interface IMMDRenderer : IRefreshable
    {
        NSBFrame Frame { get; set; }
        NSBSelection Selection { get; set; }
        NSBStyle Style { get; set; }

        RendererConfig Config { get; }

        /// <summary>
        /// Return the radius of a sphere which roughly bounds the renderering of the
        /// specified atom, or -1 if that atom is not rendered. Intended to be used for
        /// colliders or rendering extra highlights or outlines for arbitrary renderers.
        /// </summary>
        float GetAtomBoundingRadius(int id);
    }

    /// <summary>
    /// Base code for an IMMRenderer molecular renderer. Override the Refresh method
    /// with rendering code.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class MMDRenderer : MonoBehaviour, IMMDRenderer
    {
        public bool AutoRefresh;

        public NSBFrame Frame { get; set; }
        public NSBSelection Selection { get; set; }
        public NSBStyle Style { get; set; }

        public virtual float GetAtomBoundingRadius(int id) => -1;
        public abstract RendererConfig Config { get; }

        public bool Valid => Frame != null && Selection != null && Style != null;

        protected bool CheckValidity()
        {
            if (!Valid)
            {
                //Debug.LogError($"{name}: Missing one of Frame ({Frame}), Selection ({Selection}), Style ({Style})");

                return false;
            }

            return true;
        }

        protected virtual void Update()
        {
            if (AutoRefresh)
            {
                Refresh();
            }
        }

        public virtual void Refresh()
        {
            if (!CheckValidity()) return;
        }

        /// <summary>
        /// Convenience method to find or create MeshFilter and MeshRenderer components
        /// for this renderer, and configure them to ignore lighting and shadows.
        /// </summary>
        protected void AddMesh(ref MeshFilter filter, ref MeshRenderer renderer)
        {
            filter = gameObject.GetComponent<MeshFilter>();
            if (filter == null) filter = gameObject.AddComponent<MeshFilter>();

            renderer = gameObject.GetComponent<MeshRenderer>();
            if (renderer == null) renderer = gameObject.AddComponent<MeshRenderer>();

            renderer.receiveShadows = false;
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            renderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
            renderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
        }
    }
}