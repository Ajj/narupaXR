﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using NSB.Processing;
using UnityEngine;

namespace NSB.MMD
{
    public class LiquoriceRepresentation : MMDRenderer
    {
        [System.Serializable]
        public class LiquoriceConfig : RendererConfig
        {
            public FloatParameter Radius;

            public IntParameter AtomMeshComplexity;
            public BoolParameter AtomSpin;
            public BoolParameter AtomHemispheres;
            public FloatParameter AtomHemisphereCull;
            public IntParameter BondPrismSides;
            public FloatParameter BlendSharpness;

            protected override void Setup()
            {
                Radius = AddFloat(nameof(Radius), 0.02f, 0, 0.1f, dirtying: false);

                AtomMeshComplexity = AddInt(nameof(AtomMeshComplexity), 6, 0, 12);
                AtomSpin = AddBool(nameof(AtomSpin), false);
                AtomHemispheres = AddBool(nameof(AtomHemispheres), false);
                AtomHemisphereCull = AddFloat(nameof(AtomHemisphereCull), 0, 0, 1);

                BondPrismSides = AddInt(nameof(BondPrismSides), 3, 3, 16);
                BlendSharpness = AddFloat("Blend Blending", 0.5f, 0, 1, false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private VDWRepresentation atoms;

        [SerializeField]
        private BondsRepresentation bonds;

        public LiquoriceConfig Settings = new LiquoriceConfig();
        public override RendererConfig Config => Settings;

        private NSBStyle atomStyle = new NSBStyle();

        public override void Refresh()
        {
            atomStyle.AtomColors = Style.AtomColors;
            atomStyle.GenerateAtomStyle(atomStyle.AtomRadiuses, Frame, (f, i) => Settings.Radius.Value);

            atoms.Frame = Frame;
            atoms.Selection = Selection;
            atoms.Style = atomStyle;

            atoms.Settings.MeshComplexity.Value = Settings.AtomMeshComplexity.Value;
            atoms.Settings.Spin.Value = Settings.AtomSpin.Value;
            atoms.Settings.Scaling.Value = .5f;

            bonds.Frame = Frame;
            bonds.Selection = Selection;
            bonds.Style = Style;

            bonds.Settings.EndCaps.Value = false;
            bonds.Settings.BlendSharpness.Value = Settings.BlendSharpness.Value;
            bonds.Settings.PrismSides.Value = Settings.BondPrismSides.Value;
            bonds.Settings.Radius.Value = Settings.Radius.Value;

            atoms.Refresh();
            bonds.Refresh();
        }

        public override float GetAtomBoundingRadius(int id)
        {
            if (!Selection.Contains(id)) return -1;

            return Settings.Radius.Value * .5f;
        }
    }
}