﻿using System.Collections.Generic;
using Nano.Client;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using UnityEngine;

namespace NSB.MMD.Styles
{
    public class BondGuesser : FrameFilter, IFrameSource
    {
        [Range(0, 1f)]
        public float IonicThreshold = 0.01f;
        [Range(0, 1f)]
        public float HydrogenThreshold = 0.01f;

        public NSBFrame Frame = new NSBFrame();

        NSBFrame IFrameSource.Frame => Frame;

        private List<ushort> sodiums = new List<ushort>();
        private List<ushort> chlorines = new List<ushort>();
        private List<ushort> oxygens = new List<ushort>();
        private List<ushort> hydrogens = new List<ushort>();
        private BondPair[] bonds = new BondPair[2048];

        private HashSet<BondPair> existing = new HashSet<BondPair>();

        public override void Refresh()
        {
            var frame = frameSource.Component.Frame;

            if (frame.AtomCount == 0) return;

            NSBFrame.Interpolate(frame, frame, Frame, 0);

            ushort Na = (ushort) Nano.Science.Element.Sodium;
            ushort Cl = (ushort) Nano.Science.Element.Chlorine;
            ushort O = (ushort) Nano.Science.Element.Oxygen;
            ushort H = (ushort) Nano.Science.Element.Hydrogen;

            sodiums.Clear();
            chlorines.Clear();
            oxygens.Clear();
            hydrogens.Clear();

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                if (frame.AtomTypes[i] == Na)
                {
                    sodiums.Add((ushort) i);
                }
                else if (frame.AtomTypes[i] == Cl)
                {
                    chlorines.Add((ushort) i);
                }
                else if (frame.AtomTypes[i] == O)
                {
                    oxygens.Add((ushort) i);
                }
                else if (frame.AtomTypes[i] == H)
                {
                    hydrogens.Add((ushort) i);
                }
            }

            existing.Clear();
            existing.UnionWith(frame.BondPairs);

            float check1 = IonicThreshold * IonicThreshold;
            float check2 = HydrogenThreshold * HydrogenThreshold;
            int bondCount = 0;

            for (int i = 0; i < sodiums.Count; ++i)
            {
                for (int j = 0; j < chlorines.Count; ++j)
                {
                    ushort ii = sodiums[i];
                    ushort jj = chlorines[j];

                    var pair = new BondPair(ii, jj);

                    if (Vector3.SqrMagnitude(frame.AtomPositions[ii] - frame.AtomPositions[jj]) <= check1)
                    {
                        pair.A = ii;
                        pair.B = jj;

                        bonds[bondCount] = pair;
                        bondCount += 1;
                    }
                }
            }

            for (int i = 0; i < oxygens.Count; ++i)
            {
                for (int j = 0; j < hydrogens.Count; ++j)
                {
                    ushort ii = oxygens[i];
                    ushort jj = hydrogens[j];

                    var pair = new BondPair(ii, jj);

                    if (!existing.Contains(pair)
                        && Vector3.SqrMagnitude(frame.AtomPositions[ii] - frame.AtomPositions[jj]) <= check2)
                    {
                        pair.A = ii;
                        pair.B = jj;
                        bonds[bondCount] = pair;
                        bondCount += 1;
                    }
                }
            }

            Frame.BondCount = bondCount;
            Frame.BondPairs = bonds;
        }
    }
}
