﻿using System.Collections.Generic;
using Nano.Science;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Styles
{
    public class CPKStyle : FrameFilter, IStyleSource
    {
        public NSBStyle Style = new NSBStyle();

        NSBStyle IStyleSource.Style => Style;

        public List<Color32> ElementColors = new List<Color32>();
        public List<float> ElementRadiuses = new List<float>();

        [Header("Config")]
        [Range(0, 1)]
        [SerializeField]
        private float SaturationMultiplier = 0.65f;

        [SerializeField]
        private SelectionSource highlightSource;

        protected override void Awake()
        {
            base.Awake();

            ElementColors.Resize((int)Element.MAX);
            ElementRadiuses.Resize((int)Element.MAX);

            RefreshElementStyles();
            Style.Name = name;
        }

        public override void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
            if (highlightSource.Linked)
            {
                Style.HighlightedAtoms = highlightSource.Component.Selection;
            }

            if (topologyChanged)
            {
                Profiler.BeginSample("CPKStyle.UpdateFilter");

                Style.AtomColors.Resize(frame.AtomCount);
                Style.AtomPaletteIndices.Resize(frame.AtomCount);
                Style.AtomRadiuses.Resize(frame.AtomCount);

                for (int i = 0; i < frame.AtomCount; ++i)
                {
                    ushort type = frame.AtomTypes[i];

                    Style.AtomColors[i] = ElementColors[type];
                    Style.AtomPaletteIndices[i] = (byte)type;
                    Style.AtomRadiuses[i] = ElementRadiuses[type];
                }

                Style.InitPalette();

                for (int i = 0; i < ElementColors.Count; ++i)
                {
                    Style.PaletteColors[i] = ElementColors[i];
                }

                Style.CommitPalette();
                Style.PaletteIsValid = true;

                Profiler.EndSample();
            }
        }

        [ContextMenu("Refresh")]
        public void RefreshElementStyles()
        {
            GenerateCPKElements(ElementColors, ElementRadiuses, SaturationMultiplier);
        }

        public static void GenerateCPKElements(List<Color32> colors = null,
            List<float> radiuses = null,
            float saturation = 0.75f,
            float value = 1f)
        {
            int count = (int)Element.MAX;

            colors?.Resize(count);
            radiuses?.Resize(count);

            for (int i = 0; i < count; ++i)
            {
                var element = (Element)i;

                float h, s, v;

                var properties = PeriodicTable.GetElementProperties(element);
                var color = properties.CPKColor.ParseHexColor();

                Color.RGBToHSV(color, out h, out s, out v);

                if (colors != null) colors[(int)element] = Color.HSVToRGB(h, s * saturation, v * value);
                if (radiuses != null) radiuses[(int)element] = properties.VDWRadius;
            }
        }
    }
}