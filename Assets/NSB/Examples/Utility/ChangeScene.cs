﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    public class ChangeScene : MonoBehaviour
    {
        public SceneField scene;

        public void Invoke()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
        }
    }
}
