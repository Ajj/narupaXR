﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    static class UnityHierarchyHelper
    {
        public static bool GetComponentInChildren_NoParent<T>(MonoBehaviour parent, out T found) where T : MonoBehaviour
        {
            foreach (T item in parent.GetComponentsInChildren<T>())
            {
                if (item.transform == parent.transform)
                {
                    continue;
                }

                found = item;

                return true;
            }

            found = null;

            return false;
        }

    }
}
