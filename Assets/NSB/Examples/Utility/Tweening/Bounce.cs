﻿namespace NSB.Examples.Utility.Tweening
{
    class Bounce : IEaser
    {
        public float EaseIn(float ratio)
        {
            return 1 - EaseOut(1 - ratio);
        }

        public float EaseOut(float ratio)
        {
            if (ratio < 1f / 2.75f)
            {
                return 7.5625f * ratio * ratio;
            }
            else if (ratio < 2f / 2.75f)
            {
                return 7.5625f * (ratio -= 1.5f / 2.75f) * ratio + 0.75f;
            }
            else if (ratio < 2.5f / 2.75f)
            {
                return 7.5625f * (ratio -= 2.25f / 2.75f) * ratio + 0.9375f;
            }
            else
            {
                return 7.5625f * (ratio -= 2.625f / 2.75f) * ratio + 0.984375f;
            }
        }

        public float EaseInOut(float ratio)
        {
            return ((ratio *= 2f) < 1f) ? 0.5f * EaseIn(ratio) : 0.5f * EaseOut(ratio - 1f) + 0.5f;
        }
    }
}
