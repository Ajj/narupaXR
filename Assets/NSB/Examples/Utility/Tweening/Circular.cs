﻿using System;

namespace NSB.Examples.Utility.Tweening
{
    class Circular : IEaser
    {
        public float EaseIn(float ratio)
        {
            return -((float)Math.Sqrt(1f - ratio * ratio) - 1f);
        }

        public float EaseOut(float ratio)
        {
            return (float)Math.Sqrt(1f - (ratio - 1f) * (ratio - 1f));
        }

        public float EaseInOut(float ratio)
        {
            return ((ratio *= 2f) < 1f) ? -0.5f * ((float)Math.Sqrt(1f - ratio * ratio) - 1f) : 0.5f * ((float)Math.Sqrt(1f - (ratio -= 2f) * ratio) + 1f);
        }
    }
}
