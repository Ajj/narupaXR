﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.IO;

namespace NSB.Examples.Utility
{
    public class Android
    {
        /// <summary>
        /// Prompt the android filesystem to make all files under this directory
        /// publically visible. Otherwise they will not be visible over USB until 
        /// the device is rebooted.
        /// </summary>
        [System.Diagnostics.Conditional("UNITY_ANDROID")]
        public static void RefreshFiles(string root)
        {
            foreach (string file in Directory.GetFiles(root, "*", SearchOption.AllDirectories)) 
            {
                RefreshFile (file);
            }
        }

        /// <summary>
        /// Prompt the android filesystem to make this file publically visible.
        /// Otherwise it will not be visible over USB until the device is rebooted.
        /// </summary>
        [System.Diagnostics.Conditional("UNITY_ANDROID")]
        public static void RefreshFile(string path)
        {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (!File.Exists (path))
        {
            return;
        }
            
        using (AndroidJavaClass jcUnityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer"))
        using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject> ("currentActivity"))
        using (AndroidJavaObject joContext = joActivity.Call<AndroidJavaObject> ("getApplicationContext"))
        using (AndroidJavaClass jcMediaScannerConnection = new AndroidJavaClass ("android.media.MediaScannerConnection"))
        jcMediaScannerConnection.CallStatic("scanFile", joContext, new string[] { path }, null, null);
        #endif
        }
    }
}
