﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using NSB.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSB.Examples.Utility
{
    public class ButtonEntry
    {
        public string Text;
        public Action Action;

        public ButtonEntry(string text, Action action)
        {
            Text = text;
            Action = action;
        }
    }

    public class ButtonView : InstanceView<ButtonEntry>
    {
        [SerializeField]
        private TextMeshProUGUI label;
        [SerializeField]
        private Button button;

        public override void Setup()
        {
            button.onClick.AddListener(() => config.Action());
        }

        public override void Refresh()
        {
            label.text = config.Text;
        }
    }
}