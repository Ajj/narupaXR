﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.Events;

namespace NSB.Examples.UGUI
{
    public class KeyboardHotkey : MonoBehaviour
    {
        [System.Serializable]
        public class Triggered : UnityEvent { };

        public KeyCode HotKey;
        public Triggered OnTriggered;

        public void Update()
        {
            if (Input.GetKeyDown(HotKey))
            {
                OnTriggered.Invoke();
            }
        }
    }
}
