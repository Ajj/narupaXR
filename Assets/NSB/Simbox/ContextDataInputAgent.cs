﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Client;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using SlimMath;

namespace NSB.Simbox
{
    internal class ContextIncomingDataAgent : IncomingDataAgent
    {
        public ContextIncomingDataAgent(TransportContext transportContext)
            : base(transportContext)
        {
        }

        protected override void CreateDataStream(TransportDataStreamReceiver receiver, ref TransportStreamFormatDescriptor descriptor)
        {
            if (receiver.ID == (ushort)StreamID.VRInteraction || receiver.ID == (ushort)StreamID.VRPositions)
            {
                receiver.CreateStream<VRInteraction>(ref descriptor);
                return;
            }

            if (receiver.ID == (ushort)StreamID.Interaction)
            {
                receiver.CreateStream<Nano.Transport.Variables.Interaction.Interaction>(ref descriptor);
                return;
            }

            if (receiver.ID == (ushort)StreamID.InteractionForceInfo)
            {
                receiver.CreateStream<InteractionForceInfo>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 4)
            {
                receiver.CreateStream<Half4>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 3)
            {
                receiver.CreateStream<Half3>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 4)
            {
                receiver.CreateStream<UnityEngine.Vector4>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 3)
            {
                receiver.CreateStream<UnityEngine.Vector3>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.UInt16 && descriptor.ChannelCount == 2)
            {
                receiver.CreateStream<BondPair>(ref descriptor);

                return;
            }

            if (descriptor.Type == VariableType.Bool && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<bool>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Int8 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<sbyte>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.UInt8 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<byte>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Int16 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<short>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.UInt16 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<ushort>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Int32 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<int>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.UInt32 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<uint>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Int64 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<long>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.UInt64 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<ulong>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<Half>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<float>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Double && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<double>(ref descriptor);
                return;
            }

            throw new NotImplementedException();
        }
    }
}