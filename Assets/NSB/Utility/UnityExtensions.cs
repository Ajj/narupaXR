﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using UnityEngine;

namespace NSB.Utility
{
    public static partial class GameObjectExtensions
    {
        public static void SetLayerRecursively(this GameObject obj, int layer)
        {
            obj.layer = layer;

            for (int i = 0; i < obj.transform.childCount; ++i)
            {
                obj.transform.GetChild(i).gameObject.SetLayerRecursively(layer);
            }
        }
    }

    public static partial class Extensions
    {
        /// <summary>
        /// Add/Remove elements until this list is count elements long.
        /// </summary>
        public static void Resize<T>(this List<T> list, int count)
        {
            if (count < list.Count)
            {
                list.RemoveRange(count, list.Count - count);
            }

            for (int i = list.Count; i < count; ++i)
            {
                list.Add(default(T));
            }
        }
    }
}