﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using NSB.Processing;
using NSB.Simbox.Topology;
using UnityEditor;
using UnityEngine;

namespace NSB.Debugging.Editor
{
    [CustomEditor(typeof(InspectorSelection))]
    public class InspectorSelectionEditor : UnityEditor.Editor
    {
        private int current;
        private int rangeStart, rangeEnd;

        private HashSet<AtomGroup> opened = new HashSet<AtomGroup>();

        private void DrawGroup(AtomGroup group)
        {
            EditorGUILayout.BeginHorizontal();

            bool open = opened.Contains(group);

            if (group.Children.Count > 0)
            {
                open = EditorGUILayout.Foldout(open, group.Name);
            }
            else
            {
                EditorGUILayout.PrefixLabel(group.Name);
            }

            if (open)
            {
                opened.Add(group);
            }
            else
            {
                opened.Remove(group);
            }

            if (GUILayout.Button("Select"))
            {
                foreach (var atom in group.Atoms)
                {
                    selector.Selection.Add(frame.Topology.AtomToVisibleIndexMap[atom]);
                }
            }

            if (GUILayout.Button("Deselect"))
            {
                foreach (int id in group.Atoms.Select(atom => frame.Topology.AtomToVisibleIndexMap[atom]))
                {
                    selector.Selection.Remove(id);
                }
            }

            EditorGUILayout.EndHorizontal();

            if (open)
            {
                EditorGUI.indentLevel += 1;

                foreach (var child in group.Children)
                {
                    DrawGroup(child);
                }

                EditorGUI.indentLevel -= 1;
            }
        }

        private InspectorSelection selector;
        private NSBFrame frame;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            selector = (InspectorSelection)target;
            frame = selector.FrameSource.Component.Frame;

            if (EditorGUILayout.Foldout(true, "Topology"))
            {
                frame.Topology.UpdateVisibleAtomMap(frame.VisibleAtomMap);

                foreach (var group in frame.Topology.AtomGroups.Values.Where(g => g.Parent == null))
                {
                    DrawGroup(group);
                }
            }

            EditorGUILayout.HelpBox(string.Join(", ", selector.Selection.Select(i => i.ToString()).ToArray()),
                MessageType.Info);

            selector.Selection.SetSourceAtomCount(EditorGUILayout.IntField("Source Atoms", selector.Selection.SourceAtomCount));

            if (selector.Selection.SourceAtomCount > 0 && GUILayout.Button("Invert"))
            {
                selector.Selection.Invert();
            }

            GUILayout.BeginHorizontal();

            current = EditorGUILayout.IntField(current);

            if (GUILayout.Button("Toggle"))
            {
                if (!selector.Selection.Contains(current))
                {
                    selector.Selection.Add(current);
                }
                else
                {
                    selector.Selection.Remove(current);
                }
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            rangeStart = EditorGUILayout.IntField(rangeStart);
            rangeEnd = EditorGUILayout.IntField(rangeEnd);

            if (GUILayout.Button("Add Range"))
            {
                for (int i = rangeStart; i <= rangeEnd; ++i)
                {
                    selector.Selection.Add(i);
                }
            }

            if (GUILayout.Button("Remove"))
            {
                for (int i = rangeStart; i <= rangeEnd; ++i)
                {
                    selector.Selection.Remove(i);
                }
            }

            GUILayout.EndHorizontal();

            if (GUILayout.Button("Clear"))
            {
                selector.Selection.SetAll(false);
            }
        }
    }
}